---
title: API Reference

language_tabs:
- bash

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
<!-- END_INFO -->

#api


<!-- START_2b6e5a4b188cb183c7e59558cce36cb6 -->
## api/user


> No example provided.

### HTTP Request
`GET api/user`


<!-- END_2b6e5a4b188cb183c7e59558cce36cb6 -->

#0


<!-- START_53be1e9e10a08458929a2e0ea70ddb86 -->
## /


> No example provided.

### HTTP Request
`GET /`


<!-- END_53be1e9e10a08458929a2e0ea70ddb86 -->

#login


<!-- START_66e08d3cc8222573018fed49e121e96d -->
## login


> No example provided.

### HTTP Request
`GET login`


<!-- END_66e08d3cc8222573018fed49e121e96d -->

<!-- START_ba35aa39474cb98cfb31829e70eb8b74 -->
## login


> No example provided.

### HTTP Request
`POST login`


<!-- END_ba35aa39474cb98cfb31829e70eb8b74 -->

#logout


<!-- START_e65925f23b9bc6b93d9356895f29f80c -->
## logout


> No example provided.

### HTTP Request
`POST logout`


<!-- END_e65925f23b9bc6b93d9356895f29f80c -->

#register


<!-- START_ff38dfb1bd1bb7e1aa24b4e1792a9768 -->
## register


> No example provided.

### HTTP Request
`GET register`


<!-- END_ff38dfb1bd1bb7e1aa24b4e1792a9768 -->

<!-- START_d7aad7b5ac127700500280d511a3db01 -->
## register


> No example provided.

### HTTP Request
`POST register`


<!-- END_d7aad7b5ac127700500280d511a3db01 -->

#password


<!-- START_d72797bae6d0b1f3a341ebb1f8900441 -->
## password/reset


> No example provided.

### HTTP Request
`GET password/reset`


<!-- END_d72797bae6d0b1f3a341ebb1f8900441 -->

<!-- START_feb40f06a93c80d742181b6ffb6b734e -->
## password/email


> No example provided.

### HTTP Request
`POST password/email`


<!-- END_feb40f06a93c80d742181b6ffb6b734e -->

<!-- START_e1605a6e5ceee9d1aeb7729216635fd7 -->
## password/reset/{token}


> No example provided.

### HTTP Request
`GET password/reset/{token}`


<!-- END_e1605a6e5ceee9d1aeb7729216635fd7 -->

<!-- START_cafb407b7a846b31491f97719bb15aef -->
## password/reset


> No example provided.

### HTTP Request
`POST password/reset`


<!-- END_cafb407b7a846b31491f97719bb15aef -->

<!-- START_b77aedc454e9471a35dcb175278ec997 -->
## password/confirm


> No example provided.

### HTTP Request
`GET password/confirm`


<!-- END_b77aedc454e9471a35dcb175278ec997 -->

<!-- START_54462d3613f2262e741142161c0e6fea -->
## password/confirm


> No example provided.

### HTTP Request
`POST password/confirm`


<!-- END_54462d3613f2262e741142161c0e6fea -->

#home


<!-- START_cb859c8e84c35d7133b6a6c8eac253f8 -->
## home


> No example provided.

### HTTP Request
`GET home`


<!-- END_cb859c8e84c35d7133b6a6c8eac253f8 -->


